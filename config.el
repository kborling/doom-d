;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; TODO $> doom refresh

;; These are used for a number of things, particularly for GPG configuration,
;; some email clients, file templates and snippets.
(setq user-full-name "Kevin Borling"
      user-mail-address "kborling@gmail.com")

;; Font Settings
;; Fura Code Nerd Font (Alternative)
(setq doom-font (font-spec :family "IBM Plex Mono" :size 32)
      doom-variable-pitch-font (font-spec :family "IBM Plex Mono" :height 120)
      ;; doom-unicode-font (font-spec :family "DejaVu Sans Mono")
      doom-unicode-font (font-spec :family "IBM Plex Mono")
      doom-serif-font (font-spec :family "Noto Serif" :weight 'semi-bold :width 'extra-condensed)
      doom-big-font (font-spec :family "IBM Plex Mono" :size 32))

;; doom-unicode-font
(setq display-line-numbers-width 4)
(setq-default line-spacing 4)

;; Theme Settings
(setq doom-themes-treemacs-theme "doom-colors") ; use the colorful treemacs theme

(add-to-list 'load-path "~/.doom.d/themes/")
(add-to-list 'custom-theme-load-path "~/.doom.d/themes")

(setq doom-theme 'doom-ibm-carbon)

(doom-themes-visual-bell-config)
(doom-themes-treemacs-config)
(doom-themes-org-config)

(after! doom-themes
  (setq
   doom-themes-enable-bold t
   doom-themes-enable-italic t))

;; Banners
(setq +doom-dashboard-banner-dir
      (concat doom-private-dir "banners/"))
;; Banner padding
(setq +doom-dashboard-banner-padding '(4 . 5))

;; Modeline Settings
(after! doom-modeline
  (setq doom-modeline-major-mode-color-icon t)
  (setq doom-modeline-gnus t
        doom-modeline-gnus-timer 'nil)
  (setq doom-modeline-bar-width 3)
  (setq doom-modeline-height 45)
  (setq doom-modeline-github t)
  (setq doom-modeline-version nil))

;; Make compatible with tiling wm
;; (setq frame-resize-pixelwise t)
;; Show trailing whitespace
(setq show-trailing-whitespace t)

;; Editing Preferences
(setq display-line-numbers-type 'relative)
(setq-default fill-column 80)

(prefer-coding-system       'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(setq default-buffer-file-coding-system 'utf-8)

;; Reduce delat of which key
(after! which-key
  (setq which-key-idle-delay 0.5
        which-key-idle-secondary-delay 0.01
        which-key-sort-order 'which-key-key-order-alpha))

;; Set default browser
(setq browse-url-browser-function 'browse-url-firefox)
;; (setq browse-url-browser-function 'eww-browse-url)

;; Key Mappings
(map! :leader
      :n "e" #'ace-window
      :n "!" #'swiper
      :n "@" #'swiper-all
      :n "#" #'deadgrep
      ;; FIXME Replace with Ivy
      ;; :n "$" #'helm-org-rifle-directories
      :n "X" #'org-capture
      (:prefix "o"
        :n "g" #'metrics-tracker-graph
        :n "o" #'org-open-at-point
        :n "w" #'deft)
      (:prefix "f"
        :n "o" #'plain-org-wiki-helm)
      (:prefix "n"
        :n "D" #'dictionary-lookup-definition
        :n "T" #'powerthesaurus-lookup-word)
      (:prefix "s"
        :n "d" #'deadgrep
        :n "q" #'org-ql-search)
      ;; FIXME Replace with Ivy
      ;; :n "b" #'helm-org-rifle-current-buffer
      ;; :n "o" #'helm-org-rifle-org-directory
      ;; :n "." #'helm-org-rifle-directories)
      (:prefix "b"
        :n "c" #'org-board-new
        :n "e" #'org-board-open)
      (:prefix "t"
        :n "s" #'org-narrow-to-subtree
        :n "w" #'widen)
      (:prefix "/"
        :n "j" #'org-journal-search))
;; Org goto
(map! (:localleader
        (:after evil-org
          :map evil-org-mode-map
          "/" #'counsel-org-goto)))

;; Easy window navigation
(map!
 (:after evil
   :en "C-h"   #'evil-window-left
   :en "C-j"   #'evil-window-down
   :en "C-k"   #'evil-window-up
   :en "C-l"   #'evil-window-right))
;; Insert new lines above/below
(map!
 (:after evil
   :m  "] SPC" #'evil-motion-insert-newline-below
   :m  "[ SPC" #'evil-motion-insert-newline-above))
;; Dired
(map!
 (:after dired
   (:map dired-mode-map
     "C-SPC" #'peep-dired)))
;; Switch buffers for treemacs
(map!
 (:after treemacs-evil
   (:map evil-treemacs-state-map
     "C-h" #'evil-window-left
     "C-l" #'evil-window-right)))
;; Treemacs follow currently opened file
(add-hook 'treemacs-mode #'treemacs-follow-mode)

;; Create new workspace when switching projects
(setq +workspaces-on-switch-project-behavior t)

;; Python LSP
(require 'lsp-python-ms)
(add-hook 'python-mode-hook #'lsp) ; or lsp-deferred

;; for executable of language server, if it's not symlinked on your PATH
(setq lsp-python-ms-executable
      "~/python-language-server/output/bin/Release/linux-x64/publish/Microsoft.Python.LanguageServer")

;; Godot
(require 'gdscript-mode)
(setq gdscript-tabs-mode t) ;; If true, use tabs for indents. Default: t
(setq gdscript-tab-width 4) ;; Controls the width of tab-based indents

;; Rust
(require 'rustic)
(add-hook 'rustic-mode-hook #'lsp-ui-doc-mode)
(add-hook 'rustic-mode-hook #'lsp)
;; FIXME: Enable rust-analyzer after issues get fixed
;; (after! rustic
;; Use rust-analyzer instead of RLS
;; (setq rustic-lsp-server 'rust-analyzer)
;; (setq lsp-rust-analyzer-server-command '("~/.cargo/bin/rust-analyzer")))

;; Kill spellcheck for Latex (super slow)
(setq-hook! 'LaTeX-mode-hook +spellcheck-immediately nil)

;; Set ripgrep as default for ivy
(setq +ivy-project-search-engines '(rg))

;; Org
(after! org (set-popup-rule! "^Capture.*\\.org$" :side 'right :size .40 :select t :vslot 2 :ttl 3))
(after! org (set-popup-rule! "Dictionary" :side 'bottom :height .40 :width 20 :select t :vslot 3 :ttl 3))
;; (after! org (set-popup-rule! "*helm*" :side 'bottom :height .40 :select t :vslot 5 :ttl 3))
(after! org (set-popup-rule! "*ivy*" :side 'bottom :height .40 :select t :vslot 5 :ttl 3))
(after! org (set-popup-rule! "*deadgrep" :side 'bottom :height .40 :select t :vslot 4 :ttl 3))
(after! org (set-popup-rule! "*xwidget" :side 'right :size .40 :select t :vslot 5 :ttl 3))
(after! org (set-popup-rule! "*org agenda*" :side 'right :size .40 :select t :vslot 2 :ttl 3))

;; Capture templates for: TODO tasks, Notes, appointments, phone calls, meetings, org-protocol, habits, and code examples
(after! org (setq org-capture-templates
                  (quote (("t" "TODO" entry (file "~/.org/refile.org")
                           "* TODO %?\n%U\n%a\n" :clock-in t :clock-resume t)
                          ("r" "Respond" entry (file "~/.org/refile.org")
                           "* NEXT Respond to %:from on %:subject\nSCHEDULED: %t\n%U\n%a\n" :clock-in t :clock-resume t :immediate-finish t)
                          ("n" "Note" entry (file "~/.org/refile.org")
                           "* %? :NOTE:\n%U\n%a\n" :clock-in t :clock-resume t)
                          ("j" "Journal" entry(file+olp+datetree"~/.org/diary.org" "Daily Logs")
                           "* %^{thought for the day}
                  :PROPERTIES:
                  :CATEGORY: %^{category}
                  :SUBJECT:  %^{subject}
                  :MOOD:     %^{mood}
                  :END:
                  :RESOURCES:
                  :END:
                  \*What was one good thing you learned today?*:
                  - %^{whatilearnedtoday}
                  \*List one thing you could have done better*:
                  - %^{onethingdobetter}
                  \*Describe in your own words how your day was*:
                  - %?" :clock-in t :clock-resume t)
                          ("w" "org-protocol" entry (file "~/.org/refile.org")
                           "* TODO Review %c\n%U\n" :immediate-finish t)
                          ("m" "Meeting" entry (file "~/.org/refile.org")
                           "* MEETING with %? :MEETING:\n%U" :clock-in t :clock-resume t)
                          ("p" "Phone call" entry (file "~/.org/refile.org")
                           "* PHONE %? :PHONE:\n%U" :clock-in t :clock-resume t)
                          ("h" "Habit" entry (file "~/.org/refile.org")
                           "* NEXT %?\n%U\n%a\nSCHEDULED: %(format-time-string \"%<<%Y-%m-%d %a .+1d/3d>>\")\n:PROPERTIES:\n:STYLE: habit\n:REPEAT_TO_STATE: NEXT\n:END:\n")
                          ("c" "Code Example" entry (file "~/.org/refile.org")
                           "* %^{example}
                :PROPERTIES:
                :SOURCE:  %^{source|Command|Script|Code|Usage}
                :SUBJECT: %^{subject}
                :END:
                \#+BEGIN_SRC %^{lang}
                %i
                \#+END_SRC
                %?")
                          )
                         )
                  )
  )


(global-auto-revert-mode t)
;; Org Directory Settings
(setq org-directory "~/.org/")
(setq diary-file "~/.org/diary.org")

(after! org (setq org-directory "~/.org/"
                  org-image-actual-width nil
                  +org-export-directory "~/.export/"
                  org-archive-location "~/.org/archive.org::datetree/"
                  org-default-notes-file "~/.org/refile.org"
                  projectile-project-search-path '("~/")))

(after! org (setq org-agenda-diary-file "~/.org/diary.org"
                  org-agenda-use-time-grid nil
                  org-agenda-skip-scheduled-if-done t
                  org-agenda-skip-deadline-if-done t
                  org-habit-show-habits t))

;; Org Export Options
(after! org (setq org-html-head-include-scripts t
                  org-export-with-toc t
                  org-export-with-author t
                  org-export-headline-levels 5
                  org-export-with-drawers t
                  org-export-with-email t
                  org-export-with-footnotes t
                  org-export-with-latex t
                  org-export-with-section-numbers nil
                  org-export-with-properties t
                  org-export-with-smart-quotes t
                  ;; org-export-backends '(pdf ascii html latex odt pandoc)
                  ))

;; Org Keyword Sequence
(after! org (setq org-todo-keywords
                  (quote ((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d)" "FIXME(f)" "DEBUG(b)")
                          (sequence "WAITING(w@/!)" "HOLD(h@/!)" "|" "CANCELLED(c@/!)" "PHONE" "MEETING")))))

;; Org Keywords
(after! org (setq org-todo-keyword-faces
                  '(("TODO" :foreground "tomato" :weight bold)
                    ("NEXT" :foreground "violet red" :weight bold)
                    ("DONE" :foreground "slategrey" :weight bold)
                    ("WAITING" :foreground "light sea green" :weight bold)
                    ("HOLD" :foreground "#1E90FF" :weight bold)
                    ("CANCELLED" :foreground "#FF4500" :weight bold)
                    ("MEETING" :foreground "green" :weight bold)
                    ("PHONE" :foreground "green" :weight bold)
                    ("FIXME" :foreground "tomato" :weight bold)
                    ("DEBUG" :foreground "#A020F0" :weight bold))))

;; Org Filter Tasks
(after! org (setq org-todo-state-tags-triggers
                  (quote (("CANCELLED" ("CANCELLED" . t))
                          ("WAITING" ("WAITING" . t))
                          ("HOLD" ("WAITING") ("HOLD" . t))
                          (done ("WAITING") ("HOLD"))
                          ("TODO" ("WAITING") ("CANCELLED") ("HOLD"))
                          ("NEXT" ("WAITING") ("CANCELLED") ("HOLD"))
                          ("DONE" ("WAITING") ("CANCELLED") ("HOLD"))))))

;; Org Link Abbreviations
(after! org (setq org-link-abbrev-alist
                  '(("duckduckgo" . "https://duckduckgo.com/?q=%s")
                    ("google" . "https://google.com/search?q=")
                    ("gmap" . "https://maps.google.com/maps?q=%s")
                    ("gimages" . "https://google.com/images?q=%s")
                    ("youtube" . "https://youtube.com/watch?v=%s")
                    ("youtu" . "https://youtube.com/results?search_query=%s")
                    ("github" . "https://github.com/%s")
                    ("attachments" . "~/.org/.attachments/"))))

;; Org Log
(after! org (setq org-log-state-notes-insert-after-drawers nil
                  org-log-into-drawer t
                  org-log-done 'time
                  org-log-repeat 'time
                  org-log-redeadline 'note
                  org-log-reschedule 'note))

;; Make Org Look Better
(after! org (setq org-bullets-bullet-list '("◉" "○")
                  org-hide-emphasis-markers t
                  org-list-demote-modify-bullet '(("+" . "-") ("1." . "a.") ("-" . "+"))
                  org-ellipsis " ▼"))

(after! org (setq org-startup-indented t
                  org-src-tab-acts-natively t))
(add-hook 'org-mode-hook 'variable-pitch-mode)
(add-hook 'org-mode-hook 'visual-line-mode)
(add-hook 'org-mode-hook 'org-num-mode)

;; Remove empty LOGBOOK drawers on clock out
;; (defun bh/remove-empty-drawer-on-clock-out ()
;;   (interactive)
;;   (save-excursion
;;     (beginning-of-line 0)
;;     (org-remove-empty-drawer-at "LOGBOOK" (point))))

;; (add-hook 'org-clock-out-hook 'bh/remove-empty-drawer-on-clock-out 'append)

;; Org Refile

;; Targets include this file and any file contributing to the agenda - up to 9 levels deep
(setq org-refile-targets (quote ((nil :maxlevel . 9)
                                 (org-agenda-files :maxlevel . 9))))

;; Use full outline paths for refile targets - we file directly with IDO
(setq org-refile-use-outline-path t)

;; Targets complete directly with IDO
(setq org-outline-path-complete-in-steps nil)

;; Allow refile to create parent tasks with confirmation
(setq org-refile-allow-creating-parent-nodes (quote confirm))

;; Use the current window for indirect buffer display
(setq org-indirect-buffer-display 'current-window)

;;;; Refile settings
;; Exclude DONE state tasks from refile targets
(defun bh/verify-refile-target ()
  "Exclude todo keywords with a done state from refile targets"
  (not (member (nth 2 (org-heading-components)) org-done-keywords)))

(setq org-refile-target-verify-function 'bh/verify-refile-target)


;; Org Source Block (Fix background highlighting color issue with certain themes)
(custom-set-faces
 '(org-block-begin-line
   ((t (:underline "#12111E" :foreground "#ebc390" :background "#12111E"))))
 '(org-block
   ((t (:background "#12111E"))))
 '(org-block-end-line
   ((t (:overline "#12111E" :foreground "#ebc390" :background "#12111E"))))
 )

;; Org PlantUML
(use-package ob-plantuml
  :ensure nil
  :commands
  (org-babel-execute:plantuml)
  :config
  (setq org-plantuml-jar-path (expand-file-name "~/.tools/plantuml.jar")))

;; Org Mind Map
(use-package org-mind-map
  :init
  (require 'ox-org)
  ;; Uncomment the below if 'ensure-system-packages` is installed
  ;;:ensure-system-package (gvgen . graphviz)
  :config
  (setq org-mind-map-engine "dot")       ; Default. Directed Graph
  ;; (setq org-mind-map-engine "neato")  ; Undirected Spring Graph
  ;; (setq org-mind-map-engine "twopi")  ; Radial Layout
  ;; (setq org-mind-map-engine "fdp")    ; Undirected Spring Force-Directed
  ;; (setq org-mind-map-engine "sfdp")   ; Multiscale version of fdp for the layout of large graphs
  ;; (setq org-mind-map-engine "twopi")  ; Radial layouts
  ;; (setq org-mind-map-engine "circo")  ; Circular Layout
  )

;; GNU Plot
(use-package gnuplot
  :config
  (setq gnuplot-program "gnuplot"))

;; Deft
(defun my-deft/strip-quotes (str)
  (cond ((string-match "\"\\(.+\\)\"" str) (match-string 1 str))
        ((string-match "'\\(.+\\)'" str) (match-string 1 str))
        (t str)))

(defun my-deft/parse-title-from-front-matter-data (str)
  (if (string-match "^title: \\(.+\\)" str)
      (let* ((title-text (my-deft/strip-quotes (match-string 1 str)))
             (is-draft (string-match "^draft: true" str)))
        (concat (if is-draft "[DRAFT] " "") title-text))))

(defun my-deft/deft-file-relative-directory (filename)
  (file-name-directory (file-relative-name filename deft-directory)))

(defun my-deft/title-prefix-from-file-name (filename)
  (let ((reldir (my-deft/deft-file-relative-directory filename)))
    (if reldir
        (concat (directory-file-name reldir) " > "))))

(defun my-deft/parse-title-with-directory-prepended (orig &rest args)
  (let ((str (nth 1 args))
        (filename (car args)))
    (concat
     (my-deft/title-prefix-from-file-name filename)
     (let ((nondir (file-name-nondirectory filename)))
       (if (or (string-prefix-p "README" nondir)
               (string-suffix-p ".txt" filename))
           nondir
         (if (string-prefix-p "---\n" str)
             (my-deft/parse-title-from-front-matter-data
              (car (split-string (substring str 4) "\n---\n")))
           (apply orig args)))))))

(provide 'my-deft-title)

(use-package deft
  :bind (("<f8>" . deft))
  :commands (deft deft-open-file deft-new-file-named)
  :config
  (setq deft-directory "~/.org/"
        deft-auto-save-interval 0
        deft-use-filename-as-title nil
        deft-current-sort-method 'title
        deft-recursive t
        deft-extensions '("md" "txt" "org")
        deft-markdown-mode-title-level 1
        deft-file-naming-rules '((noslash . "-")
                                 (nospace . "-")
                                 (case-fn . downcase))))

;; Org Clock
(defun org-clock-switch ()
  "Switch task and go-to that task"
  (interactive)
  (setq current-prefix-arg '(12)) ; C-u
  (call-interactively 'org-clock-goto)
  (org-clock-in)
  (org-clock-goto))
(provide 'org-clock-switch)

(defun org-update-cookies-after-save()
  (interactive)
  (let ((current-prefix-arg '(4)))
    (org-update-statistics-cookies "ALL")))

(add-hook 'org-mode-hook
          (lambda ()
            (add-hook 'before-save-hook 'org-update-cookies-after-save nil 'make-it-local)))
(provide 'org-update-cookies-after-save)

;; Org Agenda
;; Do not dim blocked tasks
(setq org-agenda-dim-blocked-tasks nil)

;; Compact the block agenda view
(setq org-agenda-compact-blocks t)

;; Custom agenda command definitions
(setq org-agenda-custom-commands
      (quote (("N" "Notes" tags "NOTE"
               ((org-agenda-overriding-header "Notes")
                (org-tags-match-list-sublevels t)))
              ("h" "Habits" tags-todo "STYLE=\"habit\""
               ((org-agenda-overriding-header "Habits")
                (org-agenda-sorting-strategy
                 '(todo-state-down effort-up category-keep))))
              (" " "Agenda"
               ((agenda "" nil)
                (tags "REFILE"
                      ((org-agenda-overriding-header "Tasks to Refile")
                       (org-tags-match-list-sublevels nil)))
                (tags-todo "-CANCELLED/!"
                           ((org-agenda-overriding-header "Stuck Projects")
                            (org-agenda-skip-function 'bh/skip-non-stuck-projects)
                            (org-agenda-sorting-strategy
                             '(category-keep))))
                (tags-todo "-HOLD-CANCELLED/!"
                           ((org-agenda-overriding-header "Projects")
                            (org-agenda-skip-function 'bh/skip-non-projects)
                            (org-tags-match-list-sublevels 'indented)
                            (org-agenda-sorting-strategy
                             '(category-keep))))
                (tags-todo "-CANCELLED/!NEXT"
                           ((org-agenda-overriding-header (concat "Project Next Tasks"
                                                                  (if bh/hide-scheduled-and-waiting-next-tasks
                                                                      ""
                                                                    " (including WAITING and SCHEDULED tasks)")))
                            (org-agenda-skip-function 'bh/skip-projects-and-habits-and-single-tasks)
                            (org-tags-match-list-sublevels t)
                            (org-agenda-todo-ignore-scheduled bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-todo-ignore-deadlines bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-todo-ignore-with-date bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-sorting-strategy
                             '(todo-state-down effort-up category-keep))))
                (tags-todo "-REFILE-CANCELLED-WAITING-HOLD/!"
                           ((org-agenda-overriding-header (concat "Project Subtasks"
                                                                  (if bh/hide-scheduled-and-waiting-next-tasks
                                                                      ""
                                                                    " (including WAITING and SCHEDULED tasks)")))
                            (org-agenda-skip-function 'bh/skip-non-project-tasks)
                            (org-agenda-todo-ignore-scheduled bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-todo-ignore-deadlines bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-todo-ignore-with-date bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-sorting-strategy
                             '(category-keep))))
                (tags-todo "-REFILE-CANCELLED-WAITING-HOLD/!"
                           ((org-agenda-overriding-header (concat "Standalone Tasks"
                                                                  (if bh/hide-scheduled-and-waiting-next-tasks
                                                                      ""
                                                                    " (including WAITING and SCHEDULED tasks)")))
                            (org-agenda-skip-function 'bh/skip-project-tasks)
                            (org-agenda-todo-ignore-scheduled bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-todo-ignore-deadlines bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-todo-ignore-with-date bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-sorting-strategy
                             '(category-keep))))
                (tags-todo "-CANCELLED+WAITING|HOLD/!"
                           ((org-agenda-overriding-header (concat "Waiting and Postponed Tasks"
                                                                  (if bh/hide-scheduled-and-waiting-next-tasks
                                                                      ""
                                                                    " (including WAITING and SCHEDULED tasks)")))
                            (org-agenda-skip-function 'bh/skip-non-tasks)
                            (org-tags-match-list-sublevels nil)
                            (org-agenda-todo-ignore-scheduled bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-todo-ignore-deadlines bh/hide-scheduled-and-waiting-next-tasks)))
                (tags "-REFILE/"
                      ((org-agenda-overriding-header "Tasks to Archive")
                       (org-agenda-skip-function 'bh/skip-non-archivable-tasks)
                       (org-tags-match-list-sublevels nil))))
               nil))))

;; Clocking

;; Resume clocking task when emacs is restarted
(org-clock-persistence-insinuate)
;;
;; Show lot of clocking history so it's easy to pick items off the C-F11 list
(setq org-clock-history-length 23)
;; Resume clocking task on clock-in if the clock is open
(setq org-clock-in-resume t)
;; Change tasks to NEXT when clocking in
(setq org-clock-in-switch-to-state 'bh/clock-in-to-next)
;; Separate drawers for clocking and logs
(setq org-drawers (quote ("PROPERTIES" "LOGBOOK")))
;; Save clock data and state changes and notes in the LOGBOOK drawer
(setq org-clock-into-drawer t)
;; Sometimes I change tasks I'm clocking quickly - this removes clocked tasks with 0:00 duration
(setq org-clock-out-remove-zero-time-clocks t)
;; Clock out when moving task to a done state
(setq org-clock-out-when-done t)
;; Save the running clock and all clock history when exiting Emacs, load it on startup
(setq org-clock-persist t)
;; Do not prompt to resume an active clock
(setq org-clock-persist-query-resume nil)
;; Enable auto clock resolution for finding open clocks
(setq org-clock-auto-clock-resolution (quote when-no-clock-is-running))
;; Include current clocking task in clock reports
(setq org-clock-report-include-clocking-task t)

(setq bh/keep-clock-running nil)

(defun bh/clock-in-to-next (kw)
  "Switch a task from TODO to NEXT when clocking in.
Skips capture tasks, projects, and subprojects.
Switch projects and subprojects from NEXT back to TODO"
  (when (not (and (boundp 'org-capture-mode) org-capture-mode))
    (cond
     ((and (member (org-get-todo-state) (list "TODO"))
           (bh/is-task-p))
      "NEXT")
     ((and (member (org-get-todo-state) (list "NEXT"))
           (bh/is-project-p))
      "TODO"))))

(defun bh/find-project-task ()
  "Move point to the parent (project) task if any"
  (save-restriction
    (widen)
    (let ((parent-task (save-excursion (org-back-to-heading 'invisible-ok) (point))))
      (while (org-up-heading-safe)
        (when (member (nth 2 (org-heading-components)) org-todo-keywords-1)
          (setq parent-task (point))))
      (goto-char parent-task)
      parent-task)))

(defun bh/punch-in (arg)
  "Start continuous clocking and set the default task to the
selected task.  If no task is selected set the Organization task
as the default task."
  (interactive "p")
  (setq bh/keep-clock-running t)
  (if (equal major-mode 'org-agenda-mode)
      ;;
      ;; We're in the agenda
      ;;
      (let* ((marker (org-get-at-bol 'org-hd-marker))
             (tags (org-with-point-at marker (org-get-tags-at))))
        (if (and (eq arg 4) tags)
            (org-agenda-clock-in '(16))
          (bh/clock-in-organization-task-as-default)))
    ;;
    ;; We are not in the agenda
    ;;
    (save-restriction
      (widen)
                                        ; Find the tags on the current task
      (if (and (equal major-mode 'org-mode) (not (org-before-first-heading-p)) (eq arg 4))
          (org-clock-in '(16))
        (bh/clock-in-organization-task-as-default)))))

(defun bh/punch-out ()
  (interactive)
  (setq bh/keep-clock-running nil)
  (when (org-clock-is-active)
    (org-clock-out))
  (org-agenda-remove-restriction-lock))

(defun bh/clock-in-default-task ()
  (save-excursion
    (org-with-point-at org-clock-default-task
      (org-clock-in))))

(defun bh/clock-in-parent-task ()
  "Move point to the parent (project) task if any and clock in"
  (let ((parent-task))
    (save-excursion
      (save-restriction
        (widen)
        (while (and (not parent-task) (org-up-heading-safe))
          (when (member (nth 2 (org-heading-components)) org-todo-keywords-1)
            (setq parent-task (point))))
        (if parent-task
            (org-with-point-at parent-task
              (org-clock-in))
          (when bh/keep-clock-running
            (bh/clock-in-default-task)))))))

(defvar bh/organization-task-id "eb155a82-92b2-4f25-a3c6-0304591af2f9")

(defun bh/clock-in-organization-task-as-default ()
  (interactive)
  (org-with-point-at (org-id-find bh/organization-task-id 'marker)
    (org-clock-in '(16))))

(defun bh/clock-out-maybe ()
  (when (and bh/keep-clock-running
             (not org-clock-clocking-in)
             (marker-buffer org-clock-default-task)
             (not org-clock-resolving-clocks-due-to-idleness))
    (bh/clock-in-parent-task)))

(add-hook 'org-clock-out-hook 'bh/clock-out-maybe 'append)

(require 'org-id)
(defun bh/clock-in-task-by-id (id)
  "Clock in a task by id"
  (org-with-point-at (org-id-find id 'marker)
    (org-clock-in nil)))

(defun bh/clock-in-last-task (arg)
  "Clock in the interrupted task if there is one
Skip the default task and get the next one.
A prefix arg forces clock in of the default task."
  (interactive "p")
  (let ((clock-in-to-task
         (cond
          ((eq arg 4) org-clock-default-task)
          ((and (org-clock-is-active)
                (equal org-clock-default-task (cadr org-clock-history)))
           (caddr org-clock-history))
          ((org-clock-is-active) (cadr org-clock-history))
          ((equal org-clock-default-task (car org-clock-history)) (cadr org-clock-history))
          (t (car org-clock-history)))))
    (widen)
    (org-with-point-at clock-in-to-task
      (org-clock-in nil))))

;; The following setting shows 1 minute clocking gaps.
(setq org-agenda-clock-consistency-checks
      (quote (:max-duration "4:00"
                            :min-duration 0
                            :max-gap 0
                            :gap-ok-around ("4:00"))))

;; Sometimes I change tasks I'm clocking quickly - this removes clocked tasks with 0:00 duration
(setq org-clock-out-remove-zero-time-clocks t)

;; Agenda clock report parameters
(setq org-agenda-clockreport-parameter-plist
      (quote (:link t :maxlevel 5 :fileskip0 t :compact t :narrow 80)))

                                        ; Set default column view headings: Task Effort Clock_Summary
(setq org-columns-default-format "%80ITEM(Task) %10Effort(Effort){:} %10CLOCKSUM")

                                        ; global Effort estimate values
                                        ; global STYLE property values for completion
(setq org-global-properties (quote (("Effort_ALL" . "0:15 0:30 0:45 1:00 2:00 3:00 4:00 5:00 6:00 0:00")
                                    ("STYLE_ALL" . "habit"))))

;; Agenda log mode items to display (closed and state changes by default)
(setq org-agenda-log-mode-items (quote (closed state)))

                                        ; Tags with fast selection keys
(setq org-tag-alist (quote ((:startgroup)
                            ("@errand" . ?e)
                            ("@office" . ?o)
                            ("@home" . ?H)
                            (:endgroup)
                            ("WAITING" . ?w)
                            ("HOLD" . ?h)
                            ("PERSONAL" . ?P)
                            ("WORK" . ?W)
                            ("ORG" . ?O)
                            ("NORANG" . ?N)
                            ("crypt" . ?E)
                            ("NOTE" . ?n)
                            ("CANCELLED" . ?c)
                            ("FLAGGED" . ??))))

;; Allow setting single tags without the menu
(setq org-fast-tag-selection-single-key (quote expert))

;; For tag searches ignore tasks with scheduled and deadline dates
(setq org-agenda-tags-todo-honor-ignore-options t)

;; I want to see deadlines in the agenda 30 days before the due date.
(setq org-deadline-warning-days 30)

;; Org-mode can export tables as TAB or comma delimited formats. I set the default format to CSV with:
(setq org-table-export-default-format "orgtbl-to-csv")

;; Enable habit tracking (and a bunch of other modules)
(with-eval-after-load 'org
  (add-to-list 'org-modules 'org-habit t)
  (add-to-list 'org-modules 'org-crypt t)
  (add-to-list 'org-modules 'org-inlinetask t)
  )
;; Logging
(setq org-log-done (quote time))
(setq org-log-into-drawer t)
(setq org-log-state-notes-insert-after-drawers nil)

;; (require 'org-crypt)
;;                                         ;; Encrypt all entries before saving
;; (org-crypt-use-before-save-magic)
;; (setq org-tags-exclude-from-inheritance (quote ("crypt")))
;;                                         ;; GPG key to use for encryption
;; (setq org-crypt-key "86FCB974")
;;                                         ;; Disable the default org crypt auto-save
;; (setq org-crypt-disable-auto-save nil)

;; Org Protocal
(require 'org-protocol)
;; Add new line at the end when saving
(setq require-final-newline t)
;; Better Org Lists
(setq org-list-demote-modify-bullet (quote (("+" . "-")
                                            ("*" . "-")
                                            ("1." . "-")
                                            ("1)" . "-")
                                            ("A)" . "-")
                                            ("B)" . "-")
                                            ("a)" . "-")
                                            ("b)" . "-")
                                            ("A." . "-")
                                            ("B." . "-")
                                            ("a." . "-")
                                            ("b." . "-"))))
                                        ; Org Fontify (remove fontify error)
(setq org-src-fontify-natively nil)
                                        ; Prefer lower of uppercase
(setq org-structure-template-alist
      (quote (("s" "#+begin_src ?\n\n#+end_src" "<src lang=\"?\">\n\n</src>")
              ("e" "#+begin_example\n?\n#+end_example" "<example>\n?\n</example>")
              ("q" "#+begin_quote\n?\n#+end_quote" "<quote>\n?\n</quote>")
              ("v" "#+begin_verse\n?\n#+end_verse" "<verse>\n?\n</verse>")
              ("c" "#+begin_center\n?\n#+end_center" "<center>\n?\n</center>")
              ("l" "#+begin_latex\n?\n#+end_latex" "<literal style=\"latex\">\n?\n</literal>")
              ("L" "#+latex: " "<literal style=\"latex\">?</literal>")
              ("h" "#+begin_html\n?\n#+end_html" "<literal style=\"html\">\n?\n</literal>")
              ("H" "#+html: " "<literal style=\"html\">?</literal>")
              ("a" "#+begin_ascii\n?\n#+end_ascii")
              ("A" "#+ascii: ")
              ("i" "#+index: ?" "#+index: ?")
              ("I" "#+include %file ?" "<include file=%file markup=\"?\">"))))

;; markdown improvements
(map! :localleader
      :map markdown-mode-map
      :prefix ("i" . "Insert")
      :desc "Blockquote"    "q" 'markdown-insert-blockquote
      :desc "Bold"          "b" 'markdown-insert-bold
      :desc "Code"          "c" 'markdown-insert-code
      :desc "Emphasis"      "e" 'markdown-insert-italic
      :desc "Footnote"      "f" 'markdown-insert-footnote
      :desc "Code Block"    "s" 'markdown-insert-gfm-code-block
      :desc "Image"         "i" 'markdown-insert-image
      :desc "Link"          "l" 'markdown-insert-link
      :desc "List Item"     "n" 'markdown-insert-list-item
      :desc "Pre"           "p" 'markdown-insert-pre
      (:prefix ("h" . "Headings")
        :desc "One"   "1" 'markdown-insert-atx-1
        :desc "Two"   "2" 'markdown-insert-atx-2
        :desc "Three" "3" 'markdown-insert-atx-3
        :desc "Four"  "4" 'markdown-insert-atx-4
        :desc "Five"  "5" 'markdown-insert-atx-5
        :desc "Six"   "6" 'markdown-insert-atx-6))


;; mu4e

;; This is needed because emacs won't pick up mu4e otherwise:
(add-to-list 'load-path "/usr/share/emacs/site-lisp/mu/mu4e/")
(require 'mu4e)
(require 'org-mu4e)

(add-hook 'mu4e-view-mode-hook #'visual-line-mode)
;; Spell checking ftw.
(add-hook 'mu4e-compose-mode-hook 'flyspell-mode)
;; Use imagemagick, if available.
;; Sometimes html email is just not readable in a text based client, this lets me open the
;; email in my browser.
(add-to-list 'mu4e-view-actions '("View in browser" . mu4e-action-view-in-browser) t)

(use-package mu4e
  :config
  ;; This is a helper to help determine which account context I am in based
  ;; on the folder in my maildir the email (eg. ~/.mail/kollab) is located in.
  (defun mu4e-message-maildir-matches (msg rx)
    (when rx
      (if (listp rx)
          ;; If rx is a list, try each one for a match
          (or (mu4e-message-maildir-matches msg (car rx))
              (mu4e-message-maildir-matches msg (cdr rx)))
        ;; Not a list, check rx
        (string-match rx (mu4e-message-field msg :maildir)))))

  ;; Choose account label to feed msmtp -a option based on From header
  ;; in Message buffer; This function must be added to
  ;; message-send-mail-hook for on-the-fly change of From address before
  ;; sending message since message-send-mail-hook is processed right
  ;; before sending message.
  (defun choose-msmtp-account ()
    (if (message-mail-p)
        (save-excursion
          (let*
              ((from (save-restriction
                       (message-narrow-to-headers)
                       (message-fetch-field "from")))
               (account
                (cond
                 ((string-match "kevin@borlandia.com" from) "borlandia")
                 ((string-match "kevin@kollab.com" from) "kollab"))))
            (setq message-sendmail-extra-arguments (list '"-a" account))))))

  (when (fboundp 'imagemagick-register-types)
    (imagemagick-register-types))

  (setq mail-user-agent 'mu4e-user-agent)

  (after! mu4e
    (setq! mu4e-maildir (expand-file-name "~/.mail")  ; the rest of the mu4e folders are RELATIVE to this one
           mu4e-get-mail-command "mbsync -a"
           mu4e-update-interval 300
           mu4e-index-update-in-background t
           ;; mu4e-view-prefer-html nil
           mu4e-view-show-addresses t
           ;; mu4e-html2text-command "w3m -dump -T text/html"
           mu4e-html2text-command "iconv -c -t utf-8 | pandoc -f html -t plain"
           ;; This enabled the thread like viewing of email similar to gmail's UI.
           ;; mu4e-headers-show-threads nil
           mu4e-headers-include-related t
           mu4e-attachment-dir  "~/.mail/.attachments"
           ;; This prevents saving the email to the Sent folder since gmail will do this for us on their end.
           mu4e-sent-messages-behavior 'delete
           ;; Enable inline images.
           mu4e-view-show-images t
           mu4e-completing-read-function 'ivy-completing-read
           mu4e-compose-format-flowed t
           mu4e-change-filenames-when-moving t ;; http://pragmaticemacs.com/emacs/fixing-duplicate-uid-errors-when-using-mbsync-and-mu4e/
           ;; mu4e-split-view 'vertical
           mu4e-confirm-quit nil
           mu4e-hide-index-messages t
           ;; mu4e-compose-signature-auto-include t
           ;; Insert signature file
           ;; message-signature-file "~/.doom.d/.mailsignature"

           ;; Configure Mail sending
           user-full-name "Kevin Borling"
           message-sendmail-f-is-evil 't
           message-send-mail-function 'smtpmail-send-it
           ;; message-send-mail-function 'message-send-mail-with-sendmail
           message-citation-line-format "On %a %d %b %Y at %R, %f wrote:\n"
           message-citation-line-function 'message-insert-formatted-citation-line
           message-kill-buffer-on-exit t
           ;; Org Settings
           org-mu4e-convert-to-html t
           org-export-with-toc nil
           ))

  ;; This hook correctly modifies the \Inbox and \Starred flags on email when they are marked.
  ;; Without it refiling (archiving) and flagging (starring) email won't properly result in
  ;; the corresponding gmail action.
  (add-hook 'mu4e-mark-execute-pre-hook
            (lambda (mark msg)
              (cond ((member mark '(refile trash)) (mu4e-action-retag-message msg "-\\Inbox"))
                    ((equal mark 'flag) (mu4e-action-retag-message msg "\\Starred"))
                    ((equal mark 'unflag) (mu4e-action-retag-message msg "-\\Starred")))))

  ;; This sets up my two different context for my personal and work emails.
  (setq mu4e-contexts
        `( ,(make-mu4e-context
             :name "kollab"
             :enter-func (lambda () (mu4e-message "Switch to the kollab context"))
             :match-func (lambda (msg)
                           (when msg
                             (mu4e-message-maildir-matches msg "^/kollab")))
             :leave-func (lambda () (mu4e-clear-caches))
             :vars '((user-mail-address     . "kevin@kollab.com")
                     (user-full-name        . "Kevin Borling")
                     (smtpmail-smtp-server   . "smtp.gmail.com")
                     (smtpmail-smtp-service  . 587)
                     (smtpmail-stream-type   . starttls)
                     (smtpmail-debug-info    . t)
                     (mu4e-sent-folder      . "/kollab/[Gmail].All Mail")
                     (mu4e-drafts-folder    . "/kollab/[Gmail].Drafts")
                     (mu4e-trash-folder     . "/kollab/[Gmail].Trash")
                     (mu4e-refile-folder    . "/kollab/[Gmail].All Mail")))
           ,(make-mu4e-context
             :name "borlandia"
             :enter-func (lambda () (mu4e-message "Switch to the borlandia context"))
             :match-func (lambda (msg)
                           (when msg
                             (mu4e-message-maildir-matches msg "^/borlandia")))
             :leave-func (lambda () (mu4e-clear-caches))
             :vars '((user-mail-address     . "kevin@borlandia.com")
                     (user-full-name        . "Kevin Borling")
                     (smtpmail-smtp-server   . "smtp.gmail.com")
                     (smtpmail-smtp-service  . 587)
                     (smtpmail-stream-type   . starttls)
                     (smtpmail-debug-info    . t)
                     (mu4e-sent-folder      . "/borlandia/[Gmail].All Mail")
                     (mu4e-drafts-folder    . "/borlandia/[Gmail].Drafts")
                     (mu4e-trash-folder     . "/borlandia/[Gmail].Trash")
                     (mu4e-refile-folder    . "/borlandia/[Gmail].All Mail")))))

  ;; Use the correct account context when sending mail based on the from header.
  (setq message-sendmail-envelope-from 'header)
  (add-hook 'message-send-mail-hook 'choose-msmtp-account)

  ;; Bookmarks for common searches that I use.
  (setq mu4e-bookmarks '(("\\\\Inbox" "Inbox" ?i)
                         ("flag:unread" "Unread messages" ?u)
                         ("date:today..now" "Today's messages" ?t)
                         ("date:7d..now" "Last 7 days" ?w)
                         ("mime:image/*" "Messages with images" ?p))))
